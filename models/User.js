const mongoose = require("mongoose");




//option 2
//updated 10-18-22

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "Name is Required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
	},


	email : {
		type : String,
		required: [true, "Email is Required"]
	},


	mobileNo: {
		type: Number,
		required: [true, "Mobile Number is Required"]

	},


	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
		{
			products : [
				{
					productId : {
						type : String
					},
					productName : {
						type: String
					},
					price : {
						type: Number
					},
					quantity : {
						type: Number
					}	
				}
			],	
			totalAmount : {
				type : Number
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
	
})

module.exports = mongoose.model("User", userSchema);
