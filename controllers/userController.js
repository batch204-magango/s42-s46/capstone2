const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}



//Controller for User Registration
//new registration
module.exports.registerUser = (reqBody) => {

	// return User.findOne({email: reqBody.email}).then(result => {

	// 	if(result === null){

	// 		if(reqBody.password.length >= 8){
				let newUser = new User({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email: reqBody.email,
					mobileNo: reqBody.mobileNo,
					password: bcrypt.hashSync(reqBody.password, 10)
				});

				return newUser.save().then((user, error) => {

					if(error) {
						return false
					} else {
						return true
					}
				})
			}
	// 		else{
	// 			return false
	// 		}		
	// 	}
	// 	else{
	// 		return false
	// 	}
	// })
// }






//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				// console.log(result)
				return {
					access:auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


module.exports.getUser = (reqParams) => {


	return User.findById(reqParams.userId).then(result => {
		
		result.password = "";
		return result
	
	})

}







//retrieve profile

module.exports.getProfile = (reqBody) => {
console.log(reqBody)
	return User.findById(reqBody.id).then(result => {
		// console.log(result)
		result.password = ""
		return result
	})//.catch(error => "id not found")
}

